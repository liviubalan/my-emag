// Functions
function emgInitDom() {
    var content = `
<div id="emg-web-speech">
    <div id="info">
        <p id="info_start">Click on the microphone icon and begin speaking.</p>
        <p id="info_speak_now">Speak now.</p>
        <p id="info_no_speech">No speech was detected. You may need to adjust your
            <a href="//support.google.com/chrome/bin/answer.py?hl=en&amp;answer=1407892">
                microphone settings</a>.</p>
        <p id="info_no_microphone" style="display:none">
            No microphone was found. Ensure that a microphone is installed and that
            <a href="//support.google.com/chrome/bin/answer.py?hl=en&amp;answer=1407892">
                microphone settings</a> are configured correctly.</p>
        <p id="info_allow">Click the "Allow" button above to enable your microphone.</p>
        <p id="info_denied">Permission to use microphone was denied.</p>
        <p id="info_blocked">Permission to use microphone is blocked. To change,
            go to chrome://settings/contentExceptions#media-stream</p>
        <p id="info_upgrade">Web Speech API is not supported by this browser.
            Upgrade to <a href="//www.google.com/chrome">Chrome</a>
            version 25 or later.</p>
    </div>
    <div class="right">
        <button id="start_button" onclick="startButton(event)">
            <img id="start_img" src="{images/mic.png}" alt="Start" style="margin-top: 10px;margin-right: 10px;" width="40px;">
        </button>
    </div>
    <div id="results">
        <span id="final_span" class="final"></span>
        <span id="interim_span" class="interim"></span>
        <p>
    </div>

    <input type="hidden" id="select_dialect" value="">
    <input type="hidden" id="select_voice" value="">
    <input type="hidden" id="extension_url" value="{extension_url}">
</div>

<span id="emg_settings">{settings}</span>
`;
    content = content.replace("{images/mic.png}", chrome.extension.getURL("images/mic.png"));
    content = content.replace("{extension_url}", chrome.extension.getURL(""));
    content = content.replace("{settings}", JSON.stringify(emgSettings));
    $("body").append(content);

    var content = `
<div id="emg_widgets_pricy" class="product-highlight"></div>
`;
    $(".product-highlights-wrapper .col-lg-offset-1").prepend(content);
}

function emgScriptCreate(name) {
    var path = "js/" + name;
    path = chrome.extension.getURL(path);

    script = document.createElement("script");
    script.setAttribute("async", "false");
    script.setAttribute("src", path);
    script.setAttribute("type", "text/javascript");
    document.getElementsByTagName("head")[0].appendChild(script);
}

function emgChromeListener(message) {
    emgShareBlockShow(message.id, message.checked);
    emgShareSet(message.id, message.checked);
    $("#emg_settings").text(JSON.stringify(emgSettings));
}


// Google Chrome API event listeners
chrome.runtime.onMessage.addListener(function (message, sender) {
    emgChromeListener(message);
});


// Google Chrome API event listeners
chrome.runtime.onMessage.addListener(function (message, sender) {
    var $element = "";

    switch (message.id) {
        case "web-speech-activate":
            select_dialect.value = message.lang;
            select_voice.value = message.voice;
            $element = $("#emg-web-speech");
            $element.css({
                "display": "block"
            });
            break;
        case "emg_widgets_pricy":
            if (message.checked) {
                var pricyUrl = "https://www.pricy.ro/extension/getpricehistoryinpopup/93?currency=RON&url=" + encodeURI(window.location.href)

                $.get(pricyUrl, function (data) {
                    console.log(data.PriceHistory);
                    if (data.PriceHistory) {
                        // Load google charts
                        google.charts.load('current', {'packages': ['corechart']});
                        google.charts.setOnLoadCallback(drawChart);

                        // Draw the chart and set the chart values
                        function drawChart() {
                            var data = google.visualization.arrayToDataTable([
                                ['Task', 'Hours per Day'],
                                ['Work', 8],
                                ['Eat', 2],
                                ['TV', 4],
                                ['Gym', 2],
                                ['Sleep', 8]
                            ]);

                            // Optional; add a title and set the width and height of the chart
                            var options = {'title': 'My Average Day', 'width': 550, 'height': 400};

                            // Display the chart inside the <div> element with id="emg_widgets_pricy"
                            var chart = new google.visualization.PieChart(document.getElementById('emg_widgets_pricy'));
                            chart.draw(data, options);
                        }
                    }
                });
            }

            break;
    }
});


// Run this functions on document ready
$(document).ready(function () {
    emgInitDom();

    path = "css/dark.css";
    path = chrome.extension.getURL(path);
    $("head").append($("<link>")
        .attr("rel", "stylesheet")
        .attr("type", "text/css")
        .attr("href", path));

    emgScriptCreate("share.js");
    emgScriptCreate("inject.js");

    // Bind events
    $("#final_span").bind("DOMSubtreeModified", function () {
        var str = final_span.innerText;
        var strClean = "";

        if (emgShareAssistantKeywordMatch(pattern_info, str)) {
            strClean = emgShareAssistantTermRemove(str, pattern_info);

            // Send message to background
            chrome.runtime.sendMessage({
                "action": "sound",
                "voice": select_voice.value,
                "text": strClean
            });
        } else if (emgShareAssistantKeywordMatch(pattern_state, str)) {
            $.get("https://www.emag.ro/history/shopping?ref=ua_order_history", function (data) {
                var dom_nodes = $($.parseHTML(data));
                var text = dom_nodes.find('.order-list li:nth-child(1) h1 a').text();
                text = $.trim(text);
                var orderId = text.replace(/[^\d]/g, '');
                var status = dom_nodes.find('.order-list li:nth-child(1) .order-vendor-list-shipping-status').html();
                status = $.trim(status);
                text = "Ultima comandă plasată este " + orderId + " și are starea " + status;

                // Send message to background
                chrome.runtime.sendMessage({
                    "action": "sound",
                    "voice": select_voice.value,
                    "text": text
                });
            });
        }
    });
});
