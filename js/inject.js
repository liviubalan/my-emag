function emgShowAllBlocks() {
    var i;

    // Product page
    if (!document.querySelector(".product-highlights-wrapper .yeahIWantThisProduct")) {
        return;
    }

    for (i in emgSettings) {
        emgShareBlockShow(i, emgSettings[i]);
    }
}

// Bind events
(function () {
    var myElement = document.getElementById("emg_settings");

    myElement.addEventListener("DOMSubtreeModified", function () {
        emgSettings = emg_settings.innerText;
        emgSettings = JSON.parse(emgSettings);
        emgShareSetSettings();
    }, false);

    emgShareInit();
    emgShowAllBlocks();
})();


// Assistant
showInfo("info_start");

var final_transcript = "";
var recognizing = false;
var ignore_onend;
var start_timestamp;
var extension_url_data = extension_url.value;
var extension_url_images = extension_url_data + "images/";

if (!("webkitSpeechRecognition" in window)) {
    upgrade();
} else {
    start_button.style.display = "inline-block";
    var recognition = new webkitSpeechRecognition();
    recognition.continuous = true;
    recognition.interimResults = true;

    recognition.onstart = function () {
        recognizing = true;
        showInfo("info_speak_now");
        start_img.src = extension_url_images + "mic-animate.gif";
    };

    recognition.onerror = function (event) {
        if (event.error == "no-speech") {
            start_img.src = extension_url_images + "mic.png";
            showInfo("info_no_speech");
            ignore_onend = true;
        }
        if (event.error == "audio-capture") {
            start_img.src = extension_url_images + "mic.png";
            showInfo("info_no_microphone");
            ignore_onend = true;
        }
        if (event.error == "not-allowed") {
            if (event.timeStamp - start_timestamp < 100) {
                showInfo("info_blocked");
            } else {
                showInfo("info_denied");
            }
            ignore_onend = true;
        }
    };

    recognition.onend = function () {
        recognizing = false;
        if (ignore_onend) {
            return;
        }
        start_img.src = extension_url_images + "mic.png";
        if (!final_transcript) {
            showInfo("info_start");
            return;
        }
        showInfo("");
        if (window.getSelection) {
            window.getSelection().removeAllRanges();
            var range = document.createRange();
            range.selectNode(document.getElementById("final_span"));
            window.getSelection().addRange(range);
        }
    };

    recognition.onresult = function (event) {
        var interim_transcript = "";
        for (var i = event.resultIndex; i < event.results.length; ++i) {
            if (event.results[i].isFinal) {
                final_transcript += event.results[i][0].transcript;
            } else {
                interim_transcript += event.results[i][0].transcript;
            }
        }
        final_transcript = capitalize(final_transcript);
        final_span.innerHTML = linebreak(final_transcript);
        interim_span.innerHTML = linebreak(interim_transcript);
    };
}

function upgrade() {
    start_button.style.visibility = "hidden";
    showInfo("info_upgrade");
}

function linebreak(s) {
    var two_line = /\n\n/g;
    var one_line = /\n/g;

    return s.replace(two_line, "<p></p>").replace(one_line, "<br>");
}

function capitalize(s) {
    var first_char = /\S/;

    return s.replace(first_char, function (m) {
        return m.toUpperCase();
    });
}

function startButton(event) {
    if (recognizing) {
        recognition.stop();
        return;
    }
    final_transcript = "";
    recognition.lang = select_dialect.value;
    recognition.start();
    ignore_onend = false;
    final_span.innerHTML = "";
    interim_span.innerHTML = "";
    start_img.src = extension_url_images + "mic-slash.gif";
    showInfo("info_allow");
    start_timestamp = event.timeStamp;
}

function showInfo(s) {
    if (s) {
        for (var child = info.firstChild; child; child = child.nextSibling) {
            if (child.style) {
                child.style.display = child.id == s ? "inline" : "none";
            }
        }
        info.style.visibility = "visible";
    } else {
        info.style.visibility = "hidden";
    }
}


// Bind events
var interval = 0;
(function () {
    var myElement = document.getElementById("final_span");

    interval = setInterval(function () {
        if (myElement.innerHTML.length > 0) {
            document.querySelector("#final_span").innerText = "";

            // Stop listening in order to avoid noise recording
            document.querySelector("#start_button").click();

            setTimeout(function () {
                // Start listening in order to avoid noise recording
                document.querySelector("#start_button").click();
            }, 500);
        }
    }, 3000);

    myElement.addEventListener("DOMSubtreeModified", function () {
        clearInterval(interval);
        var str = this.innerHTML;

        if (emgShareAssistantKeywordMatch(pattern_up, str)) {
            window.scrollBy(0, -500);
        } else if (emgShareAssistantKeywordMatch(pattern_down, str)) {
            window.scrollBy(0, 500);
        } else if (emgShareAssistantKeywordMatch(pattern_add, str)) {
            document.querySelector(".product-highlights-wrapper .yeahIWantThisProduct").click();
        } else if (emgShareAssistantKeywordMatch(pattern_checkout, str)) {
            window.location = "https://www.emag.ro/cart/checkout";
        } else if (emgShareAssistantKeywordMatch(pattern_info, str)) {
            // Stop listening in order to avoid noise recording
            document.querySelector("#start_button").click();
        } else if (emgShareAssistantKeywordMatch(pattern_state, str)) {
            // Stop listening in order to avoid noise recording
            document.querySelector("#start_button").click();
        } else if (emgShareAssistantKeywordMatch(pattern_search, str)) {
            // Stop listening in order to avoid noise recording
            document.querySelector("#start_button").click();
            str = emgShareAssistantTermRemove(str, pattern_search);
            document.getElementById("searchboxTrigger").value = str;
            document.getElementsByClassName("searchbox-submit-button")[0].click();
        } else if (emgShareAssistantKeywordMatch(pattern_reset, str)) {
            document.querySelector("#final_span").innerText = "";

            // Stop listening in order to avoid noise recording
            document.querySelector("#start_button").click();

            setTimeout(function () {
                // Start listening in order to avoid noise recording
                document.querySelector("#start_button").click();
            }, 500);
        } else {
            interval = setInterval(function () {
                if (myElement.innerHTML.length > 0) {
                    document.querySelector("#final_span").innerText = "";

                    // Stop listening in order to avoid noise recording
                    document.querySelector("#start_button").click();

                    setTimeout(function () {
                        // Start listening in order to avoid noise recording
                        document.querySelector("#start_button").click();
                    }, 500);
                }
            }, 3000);
        }
    }, false);
    
    setInterval(function() {
        if (!document.querySelector("#start_img").src.match(/animate/gi)) {
            document.querySelector("#select_dialect").value = "ro-RO";
            document.querySelector("#select_voice").value = "select_voice";
            document.querySelector("#emg-web-speech").style.display = "block";
            document.querySelector("#start_button").click();
        }
    }, 1000);
})();
