// Global variables
emgThemes = [
    {
        name: "Default",
        css: "default.css",
    },
    {
        name: "Dark",
        css: "dark.css",
    }
];
emgTheme = "dark.css";
emgSettingsDefault = {
    emg_themes_benefits: true,
    emg_themes_delivery_estimates: true
};
emgSettings = emgSettingsDefault;

emgAssistantLangs =
    [["Afrikaans", ["af-ZA"]],
        ["Bahasa Indonesia", ["id-ID"]],
        ["Bahasa Melayu", ["ms-MY"]],
        ["Català", ["ca-ES"]],
        ["Čeština", ["cs-CZ"]],
        ["Deutsch", ["de-DE"]],
        ["English", ["en-AU", "Australia"],
            ["en-CA", "Canada"],
            ["en-IN", "India"],
            ["en-NZ", "New Zealand"],
            ["en-ZA", "South Africa"],
            ["en-GB", "United Kingdom"],
            ["en-US", "United States"]],
        ["Español", ["es-AR", "Argentina"],
            ["es-BO", "Bolivia"],
            ["es-CL", "Chile"],
            ["es-CO", "Colombia"],
            ["es-CR", "Costa Rica"],
            ["es-EC", "Ecuador"],
            ["es-SV", "El Salvador"],
            ["es-ES", "España"],
            ["es-US", "Estados Unidos"],
            ["es-GT", "Guatemala"],
            ["es-HN", "Honduras"],
            ["es-MX", "México"],
            ["es-NI", "Nicaragua"],
            ["es-PA", "Panamá"],
            ["es-PY", "Paraguay"],
            ["es-PE", "Perú"],
            ["es-PR", "Puerto Rico"],
            ["es-DO", "República Dominicana"],
            ["es-UY", "Uruguay"],
            ["es-VE", "Venezuela"]],
        ["Euskara", ["eu-ES"]],
        ["Français", ["fr-FR"]],
        ["Galego", ["gl-ES"]],
        ["Hrvatski", ["hr_HR"]],
        ["IsiZulu", ["zu-ZA"]],
        ["Íslenska", ["is-IS"]],
        ["Italiano", ["it-IT", "Italia"],
            ["it-CH", "Svizzera"]],
        ["Magyar", ["hu-HU"]],
        ["Nederlands", ["nl-NL"]],
        ["Norsk bokmål", ["nb-NO"]],
        ["Polski", ["pl-PL"]],
        ["Português", ["pt-BR", "Brasil"],
            ["pt-PT", "Portugal"]],
        ["Română", ["ro-RO"]],
        ["Slovenčina", ["sk-SK"]],
        ["Suomi", ["fi-FI"]],
        ["Svenska", ["sv-SE"]],
        ["Türkçe", ["tr-TR"]],
        ["български", ["bg-BG"]],
        ["Pусский", ["ru-RU"]],
        ["Српски", ["sr-RS"]],
        ["한국어", ["ko-KR"]],
        ["中文", ["cmn-Hans-CN", "普通话 (中国大陆)"],
            ["cmn-Hans-HK", "普通话 (香港)"],
            ["cmn-Hant-TW", "中文 (台灣)"],
            ["yue-Hant-HK", "粵語 (香港)"]],
        ["日本語", ["ja-JP"]],
        ["Lingua latīna", ["la"]]];

pattern_up = "mergi sus$";
pattern_down = "mergi jos$";
pattern_info = "citește-mi";
pattern_state = "stare comandă$";
pattern_add = "adaugă în coș$";
pattern_checkout = "coșul meu$";
pattern_search = "caut[ăa]";
pattern_reset = "reset";

// Functions
function emgShareInit() {
    var settings = localStorage.getItem("emg_settings");

    if (settings) {
        emgSettings = JSON.parse(settings);
    }
}

function emgShareSetSettings() {
    // Set data to local storage
    localStorage.setItem("emg_settings", JSON.stringify(emgSettings));
}

function emgShareSet(key, value) {
    emgSettings[key] = value;
    emgShareSetSettings();
}

function emgShareAnimate(element, checked) {
    if (checked) {
        element.style.display = "block";
    } else {
        element.style.display = "none";
    }
}

function emgShareBlockShow(id, checked) {
    var element = "";

    switch (id) {
        case "emg_themes_skin":
            element = document.querySelector(".page-skin-element");
            emgShareAnimate(element, checked);
            if (checked) {
                element = document.querySelector(".page-skin-outer");
                element.style.paddingTop = "168px";
            } else {
                element = document.querySelector(".page-skin-outer");
                element.style.paddingTop = "0";
            }
            break;

        case "emg_themes_benefits":
            element = document.querySelector(".product-delivery-options").parentNode;
            emgShareAnimate(element, checked);
            break;

        case "emg_themes_delivery_estimates":
            element = document.querySelector(".delivery-estimate-panel");
            emgShareAnimate(element, checked);
            break;

        case "emg_themes_social_buttons":
            element = document.querySelector(".fb-share-btn-container .pull-left");
            emgShareAnimate(element, checked);
            break;

        case "emg_themes_other_offers":
            element = document.querySelector("#main-container > section:nth-child(2) > div:nth-child(1)");
            emgShareAnimate(element, checked);
            break;

        case "emg_themes_resealed":
            element = document.querySelector("#main-container > section:nth-child(2) > div:nth-child(2)");
            emgShareAnimate(element, checked);
            break;

        case "emg_themes_other_favs":
            element = document.querySelector("#main-container > section:nth-child(10)");
            emgShareAnimate(element, checked);
            break;

        case "emg_themes_nav_history":
            element = document.querySelector(".nav-history-tabs-outer");
            emgShareAnimate(element, checked);
            element = document.querySelector("#navigation-history > div > h2");
            emgShareAnimate(element, checked);
            break;
    }
}

// Assistant
function emgShareAssistantTermRemove(string, remove) {
    return string.replace(new RegExp(remove, "i"), "");
}

function emgShareAssistantKeywordMatch(pattern, subject) {
    return subject.match(new RegExp(pattern, "i"))
}
