// Functions
function emgInit() {
    var i;
    var selected;

    // Fill themes options
    for (i = 0; i < emgThemes.length; i++) {
        selected = (emgTheme == emgThemes[i].css ? true : false);
        theme.options[i] = new Option(emgThemes[i].name, emgThemes[i].css, false, selected);
    }

    // Set checkbox checked values
    for (i in emgSettings) {
        $("#" + i).prop("checked", emgSettings[i]);
    }
}

function onChangeEmgJsCheckbox($element) {
    var id = $element.prop("id");
    var checked = $element.prop("checked");
    var message = {
        id: id,
        checked: checked
    };

    emgShareSet(id, checked);

    // Send data to current tab
    chrome.tabs.query({active: true, currentWindow: true}, function (tabs) {
        chrome.tabs.sendMessage(tabs[0].id, message);
    });
}

// Assistant
function emgAssistantUpdateCountry() {
    for (var i = select_dialect.options.length - 1; i >= 0; i--) {
        select_dialect.remove(i);
    }
    var list = emgAssistantLangs[select_language.selectedIndex];
    for (var i = 1; i < list.length; i++) {
        select_dialect.options.add(new Option(list[i][1], list[i][0]));
    }
    select_dialect.style.visibility = list[1].length == 1 ? "hidden" : "visible";
}

function emgAssistantSelectLanguageDialect() {
    for (var i = 0; i < emgAssistantLangs.length; i++) {
        select_language.options[i] = new Option(emgAssistantLangs[i][0], i);
    }
    select_language.selectedIndex = 20;
    emgAssistantUpdateCountry();
}

function emgAssistantSelectVoices() {
    chrome.tts.getVoices(function (availableVoices) {
        select_voice.options[0] = new Option("Default voice", "");

        for (var i = 1; i < availableVoices.length + 1; i++) {
            select_voice.options[i] = new Option(availableVoices[i].voiceName, availableVoices[i].voiceName);
        }
    });
}


// Run this functions on document ready
$(document).ready(function () {
    // Init
    emgShareInit();
    emgInit();

    // Bind events
    $(".emg-js-checkbox").change(function () {
        onChangeEmgJsCheckbox($(this));
    });

    emgAssistantSelectLanguageDialect();
    $("#select_language").change(function () {
        emgAssistantUpdateCountry();
    });
    emgAssistantSelectVoices();

    $(".emg-js-button").click(function () {
        var id = $(this).prop("id");
        var message = {
            "id": id
        };
        if ("web-speech-activate" == id) {
            message.lang = select_dialect.value;
            message.voice = select_voice.value;
        }

        chrome.tabs.query({active: true, currentWindow: true}, function (tabs) {
            chrome.tabs.sendMessage(tabs[0].id, message);
        });
    });

    $(".emg-js-select").change(function () {
        alert("test");
    });
});
