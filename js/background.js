chrome.runtime.onMessage.addListener(function (message, sender) {
    var options = {
        "enqueue": false,
        "lang": "ro-RO",
        "pitch": 1,
        "rate": 1,
        "volume": 1,
        "voiceName": message.voice
    };

    switch (message.action) {
        case "sound":
            chrome.tts.speak(message.text, options);
            break;
    }
});
