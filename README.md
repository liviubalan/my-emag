# My eMAG

This repo is the [client-side](https://en.wikipedia.org/wiki/Client-side)
[Google Chrome](https://en.wikipedia.org/wiki/Google_Chrome) extension for `My eMAG` project.

# PhpStorm setup

First time setup:

* Open the `Settings...` dialog (*File* > *Settings...*)
* Click *Languages & Frameworks* > *JavaScript* > `Libraries`
* Click `Download...`
* Make sure `TypeScript community stubs` is selected
* Select `chrome` from the list (you can find it quickly by just typing *chrome*)
* Click `Download and Install`
* Click `OK` to close the Settings dialog
